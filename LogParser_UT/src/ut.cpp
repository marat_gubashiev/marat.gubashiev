#include <LogParser.h>

#define BOOST_TEST_MODULE tests
#include <boost/test/included/unit_test.hpp>

const std::string non_existent_file_txt = "non_existent_file.txt";
const std::string test_file_txt = "../../../LogParser_UT/test_file.txt";
const std::string test_log_file_log = "../../../LogParser_UT/test_log_file.log";

/* utils.h begin */

BOOST_AUTO_TEST_CASE(utils_Split)
{
    std::vector<std::string> dst;
    std::vector<std::string> test0;
    std::vector<std::string> test1;
    std::vector<std::string> test2;
    
    test0 = { "It", "is", "wednesday,", "my", "dudes!" };
    utils::Split(dst, "It is wednesday, my dudes!", " ");
    BOOST_CHECK(dst == test0);

    test1 = {
        "",
        "[123]; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n",
        "[456]; 12-01-2077; 12:34:56.790; ERROR; 0; message #2\n"
    };
    utils::Split(
        dst,
        "~#[123]; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n"
        "~#[456]; 12-01-2077; 12:34:56.790; ERROR; 0; message #2\n",
        "~#"
    );
    BOOST_CHECK(dst == test1);

    test2 = {
        "[123]",
        "12-01-2077",
        "12:34:56.789",
        "INFO",
        "0",
        "message #1\n"
    };
    utils::Split(
        dst,
        "[123]; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n",
        "; "
    );
    BOOST_CHECK(dst == test2);
}

BOOST_AUTO_TEST_CASE(utils_FindSubstringInBuffer)
{
    const char* buffer;
    const char* test0;
    const char* test1;
    const char* test2;
    size_t bufferSize;
    size_t result;

    buffer =
        "==================================\n"
        "  This file was created manually\n"
        "==================================\n"
        "~#[123]; 2022-06-29; 08:50:00.000; TRACE; 0; This is just trace message!\n"
        "~#[123]; 2022-06-29; 08:50:00.001; INFO; 0; This is multiline\n"
        "    info\n"
        "    message";

    test0 = "~#";
    test1 = "this string is not in buffer";
    test2 =
        "This subtring is super long This subtring is super long This subtring is super long"
        "This subtring is super long This subtring is super long This subtring is super long"
        "This subtring is super long This subtring is super long This subtring is super long"
        "This subtring is super long This subtring is super long This subtring is super long";
    
    bufferSize = strlen(buffer);

    try
    {
        result = utils::FindSubstringInBuffer(NULL, buffer + bufferSize, test0) - buffer;
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "first is NULL") == 0);
    }

    try
    {
        result = utils::FindSubstringInBuffer(buffer, NULL, test0) - buffer;
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "last is NULL") == 0);
    }

    try
    {
        result = utils::FindSubstringInBuffer(buffer, buffer + bufferSize, NULL) - buffer;
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "substr is NULL") == 0);
    }

    result = utils::FindSubstringInBuffer(buffer, buffer + bufferSize, test0) - buffer;
    BOOST_CHECK(result == 103);

    result = utils::FindSubstringInBuffer(buffer, buffer + bufferSize, test1) - buffer;
    BOOST_CHECK(result == bufferSize);

    result = utils::FindSubstringInBuffer(buffer, buffer + bufferSize, test2) - buffer;
    BOOST_CHECK(result == bufferSize);

}

/* utils.h end */

/* FileReader.h begin */

BOOST_AUTO_TEST_CASE(FileReader_FileReader)
{
    FileReader* fileReader;

    try
    {
        fileReader = new FileReader(non_existent_file_txt);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "Unavailible to open non_existent_file.txt") == 0);
    }
    
    try
    {
        fileReader = new FileReader(test_file_txt);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    delete fileReader;
}

BOOST_AUTO_TEST_CASE(FileReader_ReadNext)
{
    FileReader* fileReader;
    char* buffer;
    size_t readSize;
    const char* test0;
    const char* test1;

    test0 = 
        "/*\r\n"
        " * Hello there! =)\r\n"
        " */\r\n";

    try
    {
        fileReader = new FileReader(test_file_txt);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    buffer = NULL;
    readSize = 0;
    buffer = fileReader->ReadNext(buffer, readSize);
    BOOST_CHECK(memcmp(buffer, test0, readSize) == 0);

    delete fileReader;
    delete[] buffer;

    try
    {
        fileReader = new FileReader(test_file_txt, 10);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    buffer = NULL;
    readSize = 0;
    test1 = test0;
    buffer = fileReader->ReadNext(buffer, readSize);
    BOOST_CHECK(memcmp(buffer, test1, readSize) == 0);
    test1 += readSize;
    buffer = fileReader->ReadNext(buffer, readSize);
    BOOST_CHECK(memcmp(buffer, test1, readSize) == 0);
    test1 += readSize;
    buffer = fileReader->ReadNext(buffer, readSize);
    BOOST_CHECK(memcmp(buffer, test1, readSize) == 0);
    test1 += readSize;

    delete fileReader;
    delete[] buffer;
}

BOOST_AUTO_TEST_CASE(FileReader_GetBufferSize)
{
    FileReader* fileReader;
    size_t bufferSize;

    try
    {
        fileReader = new FileReader(test_file_txt);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    bufferSize = fileReader->GetBufferSize();
    BOOST_CHECK(bufferSize == 29);

    delete fileReader;
}

/* FileReader.h end */

/* LogLevel.h begin */

BOOST_AUTO_TEST_CASE(LogLevel_LogLevelToString)
{
    std::string level_str;

    level_str = LogLevelToString(LogLevel::LL_EMPTY);
    BOOST_CHECK(level_str == "EMPTY");

    level_str = LogLevelToString(LogLevel::LL_TRACE);
    BOOST_CHECK(level_str == "TRACE");

    level_str = LogLevelToString(LogLevel::LL_INFO);
    BOOST_CHECK(level_str == "INFO");

    level_str = LogLevelToString(LogLevel::LL_DEBUG);
    BOOST_CHECK(level_str == "DEBUG");

    level_str = LogLevelToString(LogLevel::LL_WARN);
    BOOST_CHECK(level_str == "WARN");

    level_str = LogLevelToString(LogLevel::LL_ERROR);
    BOOST_CHECK(level_str == "ERROR");

}

BOOST_AUTO_TEST_CASE(LogLevel_StringToLogLevel)
{
    LogLevel level;

    level = StringToLogLevel("EMPTY");
    BOOST_CHECK(level == LogLevel::LL_EMPTY);

    level = StringToLogLevel("TRACE");
    BOOST_CHECK(level == LogLevel::LL_TRACE);

    level = StringToLogLevel("INFO");
    BOOST_CHECK(level == LogLevel::LL_INFO);

    level = StringToLogLevel("DEBUG");
    BOOST_CHECK(level == LogLevel::LL_DEBUG);

    level = StringToLogLevel("WARN");
    BOOST_CHECK(level == LogLevel::LL_WARN);

    level = StringToLogLevel("ERROR");
    BOOST_CHECK(level == LogLevel::LL_ERROR);

    level = StringToLogLevel("Some Random Text");
    BOOST_CHECK(level == LogLevel::LL_EMPTY);
}

/* LogLevel.h end */

/* LogLine.h begin */

BOOST_AUTO_TEST_CASE(LogLine_LogLine)
{
    LogLine* logline;

    try
    {
        logline = new LogLine("12-01-2077; 12:34:56.789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "Unavailible to parse line: split.size()") == 0);
    }

    try
    {
        logline = new LogLine("<123>; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "Unavailible to parse line: thread") == 0);
    }

    try
    {
        logline = new LogLine("[123]; 12/01/2077; 12:34:56.789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "Unavailible to parse line: date and time") == 0);
    }

    try
    {
        logline = new LogLine("[123]; 12-01-2077; 12.34.56:789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "Unavailible to parse line: date and time") == 0);
    }

    try
    {
        logline = new LogLine("[123]; 12-01-2077; 12:34:56.789; SomeCoolLevel; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "Unavailible to parse line: level") == 0);
    }

    try
    {
        logline = new LogLine("[123]; 12-01-2077; 12:34:56.789; INFO; hello; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "Unavailible to parse line: flag") == 0);
    }

    try
    {
        logline = new LogLine("[123]; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    delete logline;
}

BOOST_AUTO_TEST_CASE(LogLine_operator_insertion_operator)
{
    const std::string logLineStr = "[123]; 2022-06-29; 08:50:00.000; TRACE; 0; This is just trace message!";
    std::stringstream ss;
    LogLine logLine(logLineStr);
    ss << logLine;
    BOOST_CHECK(ss.str() == logLineStr);
}

BOOST_AUTO_TEST_CASE(LogLine_operator_not_equal)
{
    const std::string logLineStr0 = "[123]; 2022-06-29; 08:50:00.000; TRACE; 0; This is just trace message!";
    const std::string logLineStr1 = "[123]; 2022-06-29; 08:50:00.003; WARN; 0; Warn message here!";
    LogLine logLine0(logLineStr0);
    LogLine logLine1(logLineStr0);
    LogLine logLine2(logLineStr1);

    BOOST_CHECK((logLine0 != logLine1) == false);
    BOOST_CHECK((logLine0 != logLine2) == true);
}

BOOST_AUTO_TEST_CASE(LogLine_operator_equal)
{
    const std::string logLineStr0 = "[123]; 2022-06-29; 08:50:00.000; TRACE; 0; This is just trace message!";
    const std::string logLineStr1 = "[123]; 2022-06-29; 08:50:00.003; WARN; 0; Warn message here!";
    LogLine logLine0(logLineStr0);
    LogLine logLine1(logLineStr0);
    LogLine logLine2(logLineStr1);

    BOOST_CHECK((logLine0 == logLine1) == true);
    BOOST_CHECK((logLine0 == logLine2) == false);
}

BOOST_AUTO_TEST_CASE(LogLine_GetLogThread)
{
    LogLine* logline;
    std::string thread;

    try
    {
        logline = new LogLine("[123]; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    thread = logline->GetLogThread();

    BOOST_CHECK(thread == "123");

    delete logline;
}

BOOST_AUTO_TEST_CASE(LogLine_GetLogDate)
{
    LogLine* logline;
    std::string date;

    try
    {
        logline = new LogLine("[123]; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    date = logline->GetLogDate();

    BOOST_CHECK(date == "12-01-2077");

    delete logline;
}

BOOST_AUTO_TEST_CASE(LogLine_GetLogTime)
{
    LogLine* logline;
    std::string time;

    try
    {
        logline = new LogLine("[123]; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    time = logline->GetLogTime();

    BOOST_CHECK(time == "12:34:56.789");

    delete logline;
}

BOOST_AUTO_TEST_CASE(LogLine_GetLogLevel)
{
    LogLine* logline;
    LogLevel level;

    try
    {
        logline = new LogLine("[123]; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    level = logline->GetLogLevel();

    BOOST_CHECK(level == LogLevel::LL_INFO);

    delete logline;
}

BOOST_AUTO_TEST_CASE(LogLine_GetLogFlag)
{
    LogLine* logline;
    std::string flag;

    try
    {
        logline = new LogLine("[123]; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    flag = logline->GetLogFlag();

    BOOST_CHECK(flag == "0");

    delete logline;
}

BOOST_AUTO_TEST_CASE(LogLine_GetLogMessage)
{
    LogLine* logline;
    std::string message;

    try
    {
        logline = new LogLine("[123]; 12-01-2077; 12:34:56.789; INFO; 0; message #1\n");
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    message = logline->GetLogMessage();

    BOOST_CHECK(message == "message #1");

    delete logline;
}

/* LogLine.h end */

/* Parser.h begin */

BOOST_AUTO_TEST_CASE(Parser_Parser)
{
    Parser* parser;

    try
    {
        parser = new Parser(non_existent_file_txt);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(strcmp(exception.what(), "Unavailible to open non_existent_file.txt") == 0);
    }

    try
    {
        parser = new Parser(test_log_file_log);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    delete parser;
}

BOOST_AUTO_TEST_CASE(Parser_size)
{
    Parser* parser;
    size_t parsed;

    try
    {
        parser = new Parser(test_log_file_log);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }
    parsed = parser->size();

    BOOST_CHECK(parsed == 5);

    delete parser;
}

BOOST_AUTO_TEST_CASE(Parser_operator_subscript)
{
    Parser* parser;
    LogLine logline;

    try
    {
        parser = new Parser(test_log_file_log);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }
    logline = (*parser)[0];

    BOOST_CHECK(logline.GetLogThread() == "123");
    BOOST_CHECK(logline.GetLogDate() == "2022-06-29");
    BOOST_CHECK(logline.GetLogTime() == "08:50:00.000");
    BOOST_CHECK(logline.GetLogLevel() == LogLevel::LL_WARN);
    BOOST_CHECK(logline.GetLogFlag() == "0");
    BOOST_CHECK(logline.GetLogMessage() == "Warn message here!");

    delete parser;
}

BOOST_AUTO_TEST_CASE(Parser_WriteInCSV)
{
    Parser* parser;
    FileReader* fileReader;
    char* buffer;
    size_t read;
    const char* test;
    size_t result;

    try
    {
        parser = new Parser(test_log_file_log);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }

    parser->WriteInCSV(test_log_file_log + ".csv");

    buffer = NULL;
    read = 0;
    fileReader = new FileReader(test_log_file_log + ".csv");
    
    buffer = fileReader->ReadNext(buffer, read);

    test = 
        "\"123\";\"2022-06-29\";\"08:50:00.000\";\"WARN\";\"0\";\"Warn message here!\"\r\n"
        "\"123\";\"2022-06-29\";\"08:50:00.001\";\"TRACE\";\"0\";\"This is just trace message!\"\r\n"
        "\"123\";\"2022-06-29\";\"08:50:00.002\";\"DEBUG\";\"0\";\"This is just debug message\"\r\n"
        "\"123\";\"2022-06-29\";\"08:50:00.003\";\"INFO\";\"0\";\"This is multiline    info    message\"\r\n"
        "\"123\";\"2022-06-29\";\"08:50:00.004\";\"ERROR\";\"0\";\"Multiline    ERROR!    MESSAGE!\"\r\n";
    result = memcmp(buffer, test, read);

    BOOST_CHECK(result == 0);

    delete parser;
    delete[] buffer;
    delete fileReader;
}

BOOST_AUTO_TEST_CASE(Parser_Filter)
{
    Parser* parser;
    std::vector<LogLine> lines0 = {
        LogLine("[123]; 2022-06-29; 08:50:00.000; WARN; 0; Warn message here!"),
        LogLine("[123]; 2022-06-29; 08:50:00.001; TRACE; 0; This is just trace message!"),
        LogLine("[123]; 2022-06-29; 08:50:00.002; DEBUG; 0; This is just debug message"),
        LogLine("[123]; 2022-06-29; 08:50:00.003; INFO; 0; This is multiline\n    info\n    message"),
        LogLine("[123]; 2022-06-29; 08:50:00.004; ERROR; 0; Multiline\n    ERROR!\n    MESSAGE!")
    };
    std::vector<LogLine> lines1 = {
        LogLine("[123]; 2022-06-29; 08:50:00.004; ERROR; 0; Multiline\n    ERROR!\n    MESSAGE!"),
        LogLine("[123]; 2022-06-29; 08:50:00.003; INFO; 0; This is multiline\n    info\n    message"),
        LogLine("[123]; 2022-06-29; 08:50:00.002; DEBUG; 0; This is just debug message"),
        LogLine("[123]; 2022-06-29; 08:50:00.001; TRACE; 0; This is just trace message!"),
        LogLine("[123]; 2022-06-29; 08:50:00.000; WARN; 0; Warn message here!")
    };
    std::vector<LogLine> lines2 = {
        LogLine("[123]; 2022-06-29; 08:50:00.004; ERROR; 0; Multiline\n    ERROR!\n    MESSAGE!"),
        LogLine("[123]; 2022-06-29; 08:50:00.000; WARN; 0; Warn message here!"),
        LogLine("[123]; 2022-06-29; 08:50:00.002; DEBUG; 0; This is just debug message"),
        LogLine("[123]; 2022-06-29; 08:50:00.003; INFO; 0; This is multiline\n    info\n    message"),
        LogLine("[123]; 2022-06-29; 08:50:00.001; TRACE; 0; This is just trace message!")

    };
    std::vector<LogLine> lines3 = {
        LogLine("[123]; 2022-06-29; 08:50:00.001; TRACE; 0; This is just trace message!"),
        LogLine("[123]; 2022-06-29; 08:50:00.003; INFO; 0; This is multiline\n    info\n    message"),
        LogLine("[123]; 2022-06-29; 08:50:00.002; DEBUG; 0; This is just debug message"),
        LogLine("[123]; 2022-06-29; 08:50:00.000; WARN; 0; Warn message here!"),
        LogLine("[123]; 2022-06-29; 08:50:00.004; ERROR; 0; Multiline\n    ERROR!\n    MESSAGE!")
    };
    std::vector<LogLine> lines4 = {
        LogLine("[123]; 2022-06-29; 08:50:00.000; WARN; 0; Warn message here!"),
        LogLine("[123]; 2022-06-29; 08:50:00.002; DEBUG; 0; This is just debug message"),
        LogLine("[123]; 2022-06-29; 08:50:00.004; ERROR; 0; Multiline\n    ERROR!\n    MESSAGE!")
    };
    std::vector<LogLine> lines5 = {
        LogLine("[123]; 2022-06-29; 08:50:00.004; ERROR; 0; Multiline\n    ERROR!\n    MESSAGE!"),
        LogLine("[123]; 2022-06-29; 08:50:00.002; DEBUG; 0; This is just debug message"),
        LogLine("[123]; 2022-06-29; 08:50:00.000; WARN; 0; Warn message here!")
    };
    std::vector<LogLine> lines6 = {
        LogLine("[123]; 2022-06-29; 08:50:00.004; ERROR; 0; Multiline\n    ERROR!\n    MESSAGE!"),
        LogLine("[123]; 2022-06-29; 08:50:00.000; WARN; 0; Warn message here!"),
        LogLine("[123]; 2022-06-29; 08:50:00.002; DEBUG; 0; This is just debug message")
    };
    std::vector<LogLine> lines7 = {
        LogLine("[123]; 2022-06-29; 08:50:00.002; DEBUG; 0; This is just debug message"),
        LogLine("[123]; 2022-06-29; 08:50:00.000; WARN; 0; Warn message here!"),
        LogLine("[123]; 2022-06-29; 08:50:00.004; ERROR; 0; Multiline\n    ERROR!\n    MESSAGE!")
    };

    try
    {
        parser = new Parser(test_log_file_log);
    }
    catch (const std::exception& exception)
    {
        BOOST_CHECK(0 == 1);
    }
    
    BOOST_CHECK(parser->size() == lines0.size());

    BOOST_CHECK(std::equal(parser->begin(), parser->end(), lines0.begin(), lines0.end()) == true);
    
    parser->Filter({ LogLevel::LL_ALL }, false, true);
    BOOST_CHECK(std::equal(parser->begin(), parser->end(), lines1.begin(), lines1.end()) == true);

    parser->Filter({ LogLevel::LL_ALL }, true, false);
    BOOST_CHECK(std::equal(parser->begin(), parser->end(), lines2.begin(), lines2.end()) == true);

    parser->Filter({ LogLevel::LL_ALL }, true, true);
    BOOST_CHECK(std::equal(parser->begin(), parser->end(), lines3.begin(), lines3.end()) == true);

    parser->Filter({ LogLevel::LL_DEBUG, LogLevel::LL_WARN, LogLevel::LL_ERROR }, false, false);
    BOOST_CHECK(std::equal(parser->begin(), parser->end(), lines4.begin(), lines4.end()) == true);

    parser->Filter({ LogLevel::LL_DEBUG, LogLevel::LL_WARN, LogLevel::LL_ERROR }, false, true);
    BOOST_CHECK(std::equal(parser->begin(), parser->end(), lines5.begin(), lines5.end()) == true);

    parser->Filter({ LogLevel::LL_DEBUG, LogLevel::LL_WARN, LogLevel::LL_ERROR }, true, false);
    BOOST_CHECK(std::equal(parser->begin(), parser->end(), lines6.begin(), lines6.end()) == true);

    parser->Filter({ LogLevel::LL_DEBUG, LogLevel::LL_WARN, LogLevel::LL_ERROR }, true, true);
    BOOST_CHECK(std::equal(parser->begin(), parser->end(), lines7.begin(), lines7.end()) == true);

    delete parser;
}

/* Parser.h end */