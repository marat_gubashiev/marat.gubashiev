#ifndef CLI_H
#define CLI_H

#include <string>
#include <vector>
#include <atomic>
#include <thread>

#include <LogParser.h>

#include "MultithreadFileParser.h"

/**
 * @brief Class contain console line interface implementation
 * 
 */
class CLI;

class CLI
{
public:
    /**
     * @brief Construct a new CLI object
     * 
     * @param argc (arg)uments (c)ount
     * @param argv (arg)uments (v)alues
     */
    CLI(int argc, char* argv[]);
    /**
     * @brief Destroy the CLI object
     * 
     */
    ~CLI();

public:
    /**
     * @brief Parse input dirs and files
     * 
     */
    void Parse();

private:
    /**
     * @brief Method returns std::string that contains progress bar
     * Default progress bars:
     * - delimiters and percents:
     * "[XXXXXXXXXX          ]  50.00%"
     * - delimiters:
     * "[XXXXXXXXXX          ]"
     * - percents:
     * "XXXXXXXXXX            50.00%"
     * - none:
     * "XXXXXXXXXX          "
     * 
     * @param addPercentages add percentages to the progress bar
     * @param len length of the progress bar
     * @param fill char to fill
     * @param unfill char to unfill
     * @param useDelimiters add delimiters to the progress bar
     * @param delimiterLeft left delimiter
     * @param delimiterRight right delimiter
     * @return std::string progress bar
     */
    std::string ProgressBar(
        bool addPercentages = true,
        size_t len = 20,
        char fill = 'X',
        char unfill = ' ',
        bool useDelimiters = true,
        char delimiterLeft = '[',
        char delimiterRight = ']'
    );

private:
    std::vector<std::string> m_pathsToDirs;
    std::vector<std::string> m_pathsToFiles;
    bool m_recursive;

    std::vector<std::string> m_filenames;
    MultithreadFileParser* m_mtfp;
};

#endif