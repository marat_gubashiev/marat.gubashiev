#ifndef MULTITHREADFILEPARSER_H
#define MULTITHREADFILEPARSER_H

#include <string>
#include <vector>
#include <atomic>
#include <thread>
#include <numeric>
#include <mutex>

#include <LogParser.h>

/**
 * @brief Class contain multithread file parser implementation
 * 
 */
class MultithreadFileParser;

class MultithreadFileParser
{
public:
    /**
     * @brief Construct a new Multithread File Parser object
     * 
     * @param filenames 
     */
    MultithreadFileParser(const std::vector<std::string>& filenames);
    /**
     * @brief Destroy the Multithread File Parser object
     * 
     */
    ~MultithreadFileParser();

public:
    /**
     * @brief Method return count of finished threads
     * 
     * @return size_t count of finished threads
     */
    inline size_t Done()
    {
        return std::accumulate<std::vector< std::atomic<bool> >::iterator, size_t>(m_allDone.begin(), m_allDone.end(), 0Ui64);
    }
    /**
     * @brief Method return is all threads finished
     * 
     * @return true all threads finished
     * @return false one or more threads are still running
     */
    inline bool IsDone() { return Done() == m_allDone.size(); }
    /**
     * @brief Method return ratio of finished to all threads [0.0 .. 1.0]
     * 
     * @return double ratio of finished to all threads
     */
    inline double DonePercentage() { return static_cast<double>(Done()) / static_cast<double>(m_allDone.size()); }
    /**
     * @brief Get the sum
     * 
     * @return size_t sum of log lines
     */
    inline size_t GetSum() { return m_sum; }
private:
    unsigned int m_hardwareConcurrency;
    unsigned int m_threadCount;
    std::vector< std::atomic<bool> > m_allDone;
    std::vector<std::thread> m_threads;
    size_t m_sum;
    std::mutex m_sumMutex;
};

#endif // MULTITHREADFILEPARSER_H