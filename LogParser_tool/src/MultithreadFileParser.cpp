#include "MultithreadFileParser.h"

#include <algorithm>

MultithreadFileParser::MultithreadFileParser(const std::vector<std::string>& filenames) :
    m_hardwareConcurrency(std::thread::hardware_concurrency()),
    m_threadCount(std::min(static_cast<size_t>(m_hardwareConcurrency), filenames.size())),
    m_sum(0)
{
    m_allDone = std::vector< std::atomic<bool> >(m_threadCount);
    m_threads = std::vector<std::thread>(m_threadCount);

    if (!m_hardwareConcurrency)
    {
        // single thread
        std::cerr << "[!] Single thread" << std::endl;
        for (size_t i = 0; i < filenames.size(); ++i)
        {
            m_sum += Parser(filenames[i]).size();
            m_allDone[i] = true;
        }
        return;
    }

    size_t filesPerThread = filenames.size() / m_threadCount;
    filesPerThread += static_cast<size_t>((filenames.size() - filesPerThread * m_threadCount) > 0); // round up

    // multi thread
    std::cerr << "[ ] Multi thread: " << m_threadCount << " out of " << m_hardwareConcurrency << " will be used. " << filesPerThread << " files per thread" << std::endl;

    for (size_t i = 0; i < m_threadCount; ++i)
    {
        m_threads[i] = std::thread(
            [&filenames, i, filesPerThread, this]()
            {
                size_t j = i * filesPerThread;
                if (j >= filenames.size())
                {
                    m_allDone[i] = true;
                    return;
                }
                for (; (j < (i + 1) * filesPerThread) && (j < filenames.size()); ++j)
                {
                    m_sumMutex.lock();
                    try
                    {
                        m_sum += Parser(filenames[j]).size();
                    }
                    catch(const std::exception& e)
                    {
                        std::cerr << "[!] Thread " << std::this_thread::get_id() << ": " << e.what() << std::endl;
                    }
                    m_sumMutex.unlock();
                    m_allDone[i] = true;
                } // for (; (j < (i + 1) * filesPerThread) && (j < filenames.size()); ++j)
            }
        );

        if (m_threads[i].joinable())
        {
            m_threads[i].join();
        }
        else
        {
            std::cerr << "[!] Thread " << i << " is not joinable" << std::endl;
            m_allDone[i] = true;
        }
    } // for (size_t i = 0; i < m_threadCount; ++i)
}

MultithreadFileParser::~MultithreadFileParser()
{}