#include "CLI.h"

#include <iostream>
#include <filesystem>

#include <boost/program_options.hpp>

#include <utils.h>

CLI::CLI(int argc, char* argv[])
{
    namespace fs = std::filesystem;
    namespace po = boost::program_options;
    po::options_description desc("General options");
    desc.add_options()
        ("help,h", "Show help")
        ("dir,d", po::value< std::vector<std::string> >(&m_pathsToDirs), "Input directory (default path is \"./\")")
        ("file,f", po::value< std::vector<std::string> >(&m_pathsToFiles), "Input file" )
        ("recursive,r", "Recursive directory traversal");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help"))
    {
        std::cout << desc << std::endl;
        exit(0);
    }

    if (!vm.count("dir") && !vm.count("file"))
    {
        std::cerr << "[!] Use \"./LogParser_tool --help\" to read options" << std::endl;
        std::cerr << "[!] Selected folder \"./\"" << std::endl;
    }

    if ((m_pathsToDirs.size() == 0) && (m_pathsToFiles.size() == 0))
    {
        m_pathsToDirs.push_back(".");
    }

    m_recursive = vm.count("recursive");

    if (m_recursive)
    {
        for (const auto& pathToDir : m_pathsToDirs)
            for (const auto& entry : fs::recursive_directory_iterator(pathToDir))
                if (entry.path().extension().string() == ".log")
                    if (!utils::IsContains(m_filenames, entry.path().string()))
                    {
                        m_filenames.push_back(entry.path().string());
                    }
    }
    else
    {
        for (const auto& pathToDir : m_pathsToDirs)
            for (const auto& entry : fs::directory_iterator(pathToDir))
                if (entry.path().extension().string() == ".log")
                    if (!utils::IsContains(m_filenames, entry.path().string()))
                    {
                        m_filenames.push_back(entry.path().string());
                    }
    }
    m_pathsToDirs.clear();

    for (const auto& pathToFile : m_pathsToFiles)
    {
        if (!utils::IsContains(m_filenames, pathToFile))
        {
            m_filenames.push_back(pathToFile);
        }
    }
    m_pathsToFiles.clear();

    if (m_filenames.size() == 0)
    {
        std::cerr << "[!] No \"*.log\" files!" << std::endl;
        exit(0);
    }
}

CLI::~CLI()
{
    m_pathsToDirs.clear();
    m_pathsToFiles.clear();

    m_filenames.clear();
    
    delete m_mtfp;
}

void CLI::Parse()
{
    m_mtfp = new MultithreadFileParser(m_filenames);

    do
    {
        std::cout << '\r' << ProgressBar();
    } while (!m_mtfp->IsDone());
    std::cout << std::endl;

    std::cout << "Total: " << m_mtfp->GetSum() << std::endl;
}

std::string CLI::ProgressBar(
    bool addPercentages,
    size_t len,
    char fill,
    char unfill,
    bool useDelimiters,
    char delimiterLeft,
    char delimiterRight
)
{
    std::string progressbar(len + (useDelimiters ? 2 : 0), unfill);
    if (useDelimiters)
    {
        progressbar[0] = delimiterLeft;
        progressbar[len + 1] = delimiterRight;
    }

    double percent = m_mtfp->DonePercentage();
    size_t fillSize = static_cast<size_t>(percent * len);
    for (size_t i = 0; i < fillSize; ++i)
        progressbar[i + useDelimiters] = fill;
    if (!addPercentages)
        return progressbar;

    std::stringstream result;
    result << progressbar
        << ' '
        << std::fixed
        << std::setprecision(2)
        << std::setw(6)
        << percent * 100.0
        << '%';
    return result.str();
}