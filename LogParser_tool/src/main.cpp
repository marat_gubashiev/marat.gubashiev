#include <iostream>
#include <string>

#include <vector>
#include <thread>
#include <filesystem>

#include <boost/program_options.hpp>

#include <LogParser.h>

#include "CLI.h"


int main(int argc, char* argv[])
try
{
    CLI cli(argc, argv);
    cli.Parse();
    return 0;
}
catch (const std::exception& e)
{
    std::cerr << e.what() << std::endl;
}