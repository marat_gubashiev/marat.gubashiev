#include "Parser.h"

Parser::Parser() :
    m_trace(false),
    m_info(false),
    m_debug(false),
    m_warn(false),
    m_error(false),
    m_sortByLevel(false),
    m_reverse(false)
{
}

Parser::Parser(const std::string& fileName) :
    m_fileName(fileName),
    m_trace(true),
    m_info(true),
    m_debug(true),
    m_warn(true),
    m_error(true),
    m_sortByLevel(false),
    m_reverse(false)
{
    FileReader* fileReader = new FileReader(fileName);
    char* buffer = NULL;
    size_t readSize = 1;
    size_t begin = 0;
    size_t end;
    size_t delimeterSize = strlen("~#");
    size_t block = 0;
    bool delimeterSplited = false;
    bool lineSplited = false;
    std::vector<std::string> lines;
    CREATE_TIMER(parser);

    START_TIMER(parser);
    while (1)
    {
        buffer = fileReader->ReadNext(buffer, readSize);

        if (!readSize)
            break;
        
        begin = 0;
        end = 0;
        lineSplited = true;
        
        if (delimeterSplited)
        {
            delimeterSplited = false;
            if (buffer[0] == '#')
            {
                begin = 1;
                lineSplited = false;
            }
        }

        while (1)
        {
            end = utils::FindSubstringInBuffer(buffer + begin, buffer + readSize, "~#") - buffer;

            /*
                line splited:
                .~#...]  [...~#
                block 1  block 2
            */
            if (block && end && lineSplited)
            {
                lines[lines.size() - 1] += std::string(buffer, end);
                lineSplited = false;
            }
            else
            {
                lines.push_back(std::string(buffer + begin, end - begin));
            }

            if (end >= readSize)
                break;
            
            begin = end + delimeterSize;
        }
        ++block;

        /*
            delimeter splited:
            .....~]  [#.....
            block 1  block 2
        */
        delimeterSplited = buffer[readSize - 1] == '~';
    }

    for (auto& line : lines)
    {
        try
        {
            m_parsed.push_back(LogLine(line));
            ++(m_parserResults.m_parsedSuccesfully);
            switch (m_parsed[m_parsed.size() - 1].GetLogLevel())
            {
            case (LogLevel::LL_TRACE):
                ++(m_parserResults.m_countOfTrace);
                break;
            case (LogLevel::LL_INFO):
                ++(m_parserResults.m_countOfInfo);
                break;
            case (LogLevel::LL_DEBUG):
                ++(m_parserResults.m_countOfDebug);
                break;
            case (LogLevel::LL_WARN):
                ++(m_parserResults.m_countOfWarn);
                break;
            case (LogLevel::LL_ERROR):
                ++(m_parserResults.m_countOfError);
                break;
            default:
                break;
            }
        }
        catch (const std::exception& exception)
        {}
    } // for (auto& line : lines)
    STOP_TIMER(parser);

    lines.clear();

    m_filtered = m_parsed;
    
    delete[] buffer;
    delete fileReader;

    m_parserResults.m_elapsedTime = TIMER_IN_SEC(parser);
    if (m_parsed.size() > 0)
    {
        m_parserResults.m_beginTime = m_parsed[0].GetLogDate() + " " + m_parsed[0].GetLogTime();
        m_parserResults.m_endTime = m_parsed[m_parsed.size() - 1].GetLogDate() + " " + m_parsed[m_parsed.size() - 1].GetLogTime();
    }
    else
    {
        m_parserResults.m_beginTime = "01-01-1970 00:00:00.000";
        m_parserResults.m_endTime = "01-01-1970 00:00:00.000";
    }
}

Parser::~Parser()
{
    m_parsed.clear();
    m_filtered.clear();
}

size_t Parser::size() const
{
    return m_filtered.size();
}

LogLine& Parser::operator[](size_t index)
{
    if (index > m_filtered.size())
        throw std::out_of_range(
            std::string("Attempt to access elements out of defined range! index: ") + 
            std::to_string(index) +
            std::string(" size: ") +
            std::to_string(m_filtered.size())
        );
    return m_filtered[index];
}

const LogLine& Parser::operator[](size_t index) const
{
    if (index > m_filtered.size())
        throw std::out_of_range(
            std::string("Attempt to access elements out of defined range! index: ") + 
            std::to_string(index) +
            std::string(" size: ") +
            std::to_string(m_filtered.size())
        );
    return m_filtered[index];
}

Parser::iterator Parser::begin()
{
    return m_filtered.begin();
}

Parser::iterator Parser::end()
{
    return m_filtered.end();
}

void Parser::Filter(
    bool trace,
    bool info,
    bool debug,
    bool warn,
    bool error,
    bool sortByLevel,
    bool reverse
)
{
    bool levels = (
        (m_trace == trace)
        && (m_info == info)
        && (m_debug == debug)
        && (m_warn == warn)
        && (m_error == error)
    );
    
    // This condition check is new filter is the same
    // If true then to filter again will be useless
    if (levels && (m_sortByLevel == sortByLevel) && (m_reverse == reverse))
    {
        return;
    }

    // This condition check is new filter is the same, besides filter flag
    // If true then to filter again will be useless, but reverse need.
    if (levels && (m_sortByLevel == sortByLevel))
    {
        std::reverse(m_filtered.begin(), m_filtered.end());
        m_reverse = reverse;
        return;
    }

    m_filtered.clear();

    for (auto& logLine : m_parsed)
    {
        switch (logLine.GetLogLevel())
        {
        case LogLevel::LL_TRACE:
            if (trace)
                m_filtered.push_back(logLine);
            break;
        case LogLevel::LL_INFO:
            if (info)
                m_filtered.push_back(logLine);
            break;
        case LogLevel::LL_DEBUG:
            if (debug)
                m_filtered.push_back(logLine);
            break;
        case LogLevel::LL_WARN:
            if (warn)
                m_filtered.push_back(logLine);
            break;
        case LogLevel::LL_ERROR:
            if (error)
                m_filtered.push_back(logLine);
            break;
        default:
            break;
        } // switch (logLine.GetLogLevel())
    } // for (auto& logLine : m_parsed)

    if (sortByLevel)
    {
        std::sort(
            m_filtered.begin(),
            m_filtered.end(),
            [&](LogLine& a, LogLine& b)
            {
                return a.GetLogLevel() > b.GetLogLevel();
            }
        );
    }

    if (reverse)
        std::reverse(m_filtered.begin(), m_filtered.end());
    
    m_trace = trace;
    m_info = info;
    m_debug = debug;
    m_warn = warn;
    m_error = error;
    m_sortByLevel = sortByLevel;
    m_reverse = reverse;
}

void Parser::Filter(const std::vector<LogLevel>& levels, bool sortByLevel, bool reverse)
{
    bool trace = false;
    bool info = false;
    bool debug = false;
    bool warn = false;
    bool error = false;

    for (auto& level : levels)
    {
        switch (level)
        {
        case LogLevel::LL_TRACE:
            trace = true;
            break;
        case LogLevel::LL_INFO:
            info = true;
            break;
        case LogLevel::LL_DEBUG:
            debug = true;
            break;
        case LogLevel::LL_WARN:
            warn = true;
            break;
        case LogLevel::LL_ERROR:
            error = true;
            break;
        case LogLevel::LL_ALL:
            trace = true;
            info = true;
            debug = true;
            warn = true;
            error = true;
            goto exit_loop;
        default:
            break;
        }
    } // for (auto& level : levels)
    exit_loop:

    Filter(trace, info, debug, warn, error, sortByLevel, reverse);
}

void Parser::WriteInCSV(const std::string& csvFileName)
{
    std::ofstream csv((csvFileName == "") ? (m_fileName + ".csv") : csvFileName, std::ios_base::out);

    if (!csv.is_open())
    {
        throw std::runtime_error("Unavailible to open " + (csvFileName == "") ? (m_fileName + ".csv") : csvFileName);
    }

    for (auto& logLine : m_filtered)
    {
        csv << '"' << logLine.GetLogThread() << "\";";
        csv << '"' << logLine.GetLogDate() << "\";";
        csv << '"' << logLine.GetLogTime() << "\";";
        csv << '"' << logLine.GetLogLevel() << "\";";
        csv << '"' << logLine.GetLogFlag() << "\";";
        csv << '"' << logLine.GetLogMessage() << '"' << std::endl;
    }
    csv.close();
}

std::ostream& operator<<(std::ostream& os, const Parser& parser)
{
    os << std::fixed;
    os << std::endl;
    os << "================= PARSER RESULTS =================" << std::endl;
    os << "File: " << parser.m_fileName << std::endl;
    os << "Parsed: " << parser.m_parserResults.m_parsedSuccesfully << std::endl;
    os << "Parsed in: " << std::setprecision(3) << parser.m_parserResults.m_elapsedTime << " sec" << std::endl;
    os << "Parser speed: " << std::setprecision(3) << parser.m_parserResults.m_parsedSuccesfully / parser.m_parserResults.m_elapsedTime << " logs per sec" << std::endl;
    os << "==================================================" << std::endl;
    os << "TRACE: " << parser.m_parserResults.m_countOfTrace << std::endl;
    os << "INFO : " << parser.m_parserResults.m_countOfInfo << std::endl;
    os << "DEBUG: " << parser.m_parserResults.m_countOfDebug << std::endl;
    os << "WARN : " << parser.m_parserResults.m_countOfWarn << std::endl;
    os << "ERROR: " << parser.m_parserResults.m_countOfError << std::endl;
    os << "==================================================" << std::endl;
    os << "Begin: " << parser.m_parserResults.m_beginTime << std::endl;
    os << "End  : " << parser.m_parserResults.m_endTime << std::endl;
    os << "==================================================" << std::endl;
    os << "Filter: " << std::endl;
    if (parser.m_trace)
        os << "    LL_TRACE" << std::endl;
    if (parser.m_info)
        os << "    LL_INFO" << std::endl;
    if (parser.m_debug)
        os << "    LL_DEBUG" << std::endl;
    if (parser.m_warn)
        os << "    LL_WARN" << std::endl;
    if (parser.m_error)
        os << "    LL_ERROR" << std::endl;
    if (parser.m_sortByLevel)
        os << "    Sorted by level" << std::endl;
    else
        os << "    Sorted by time" << std::endl;
    if (parser.m_reverse)
        os << "    Reversed" << std::endl;
    else
        os << "    Not reversed" << std::endl;
    os << "==================================================" << std::endl;
    return os;
}

std::ostream& operator<<(std::ostream& os, const std::unique_ptr<Parser>& parser_ptr)
{
    os << *parser_ptr.get();
    return os;
}