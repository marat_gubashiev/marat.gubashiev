#include "LogLine.h"

LogLine::LogLine() :
    m_thread(""),
    m_date(""),
    m_time(""),
    m_level(LogLevel::LL_EMPTY),
    m_message("")
{}

LogLine::LogLine(std::string& line)
{
    std::vector<std::string> split;
    size_t thread;
    int dateParsedElements;
    int timeParsedElements;
    int tmp;

    utils::Split(split, line, "; ");
    line.clear();
    
    if (split.size() != 6)
        throw std::runtime_error("Unavailible to parse line: split.size()");
    
    if (sscanf(split[0].c_str(), "[%zu]", &thread) != 1)
        throw std::runtime_error("Unavailible to parse line: thread");

    dateParsedElements = sscanf(split[1].c_str(), "%d-%d-%d", &tmp, &tmp, &tmp);
    timeParsedElements = sscanf(split[2].c_str(), "%d:%d:%d.%d", &tmp, &tmp, &tmp, &tmp);
    if (
        (dateParsedElements != DATE_PARSED_ELEMENTS)
        || (timeParsedElements != TIME_PARSED_ELEMENTS)
    )
        throw std::runtime_error("Unavailible to parse line: date and time");

    if (!static_cast<int>(m_level = StringToLogLevel(split[3])))
        throw std::runtime_error("Unavailible to parse line: level");

    if (sscanf(split[4].c_str(), "%d", &tmp) != 1)
        throw std::runtime_error("Unavailible to parse line: flag");

    m_thread = std::to_string(thread);
    m_date = split[1];
    m_time = split[2];
    // m_level initialized here: if (!static_cast<int>(m_level = StringToLogLevel(split[3])))
    m_flag = split[4];
    m_message = split[5];

    m_message.erase(std::remove(m_message.begin(), m_message.end(), '\n'), m_message.end());
}

LogLine::LogLine(const std::string& constLine)
{
    std::vector<std::string> split;
    size_t thread;
    int dateParsedElements;
    int timeParsedElements;
    int tmp;

    utils::Split(split, constLine, "; ");
    
    if (split.size() != 6)
        throw std::runtime_error("Unavailible to parse line: split.size()");
    
    if (sscanf(split[0].c_str(), "[%zu]", &thread) != 1)
        throw std::runtime_error("Unavailible to parse line: thread");

    dateParsedElements = sscanf(split[1].c_str(), "%d-%d-%d", &tmp, &tmp, &tmp);
    timeParsedElements = sscanf(split[2].c_str(), "%d:%d:%d.%d", &tmp, &tmp, &tmp, &tmp);
    if ((dateParsedElements != DATE_PARSED_ELEMENTS) ||
        (timeParsedElements != TIME_PARSED_ELEMENTS))
        throw std::runtime_error("Unavailible to parse line: date and time");

    if (!static_cast<int>(m_level = StringToLogLevel(split[3])))
        throw std::runtime_error("Unavailible to parse line: level");

    if (sscanf(split[4].c_str(), "%d", &tmp) != 1)
        throw std::runtime_error("Unavailible to parse line: flag");

    m_thread = std::to_string(thread);
    m_date = split[1];
    m_time = split[2];
    // m_level initialized here: if (!static_cast<int>(m_level = StringToLogLevel(split[3])))
    m_flag = split[4];
    m_message = split[5];
    
    m_message.erase(std::remove(m_message.begin(), m_message.end(), '\n'), m_message.end());
}


LogLine::~LogLine()
{
    m_thread.clear();
    m_date.clear();
    m_time.clear();
    m_level = LogLevel::LL_EMPTY;
    m_flag.clear();
    m_message.clear();
}

std::ostream& operator<<(std::ostream& os, const LogLine& logLine)
{
    os <<
        "[" << logLine.m_thread << "]; " << // thread
        logLine.m_date << "; " << // date
        logLine.m_time << "; " << // time
        logLine.m_level << "; " << // level
        logLine.m_flag << "; " << // flag
        logLine.m_message; // message
    return os;
}

bool LogLine::operator!=(const LogLine& logLine)
{
    return (
        (m_thread != logLine.m_thread)
        || (m_date != logLine.m_date)
        || (m_time != logLine.m_time)
        || (m_level != logLine.m_level)
        || (m_flag != logLine.m_flag)
        || (m_message != logLine.m_message)
    );
}

bool LogLine::operator==(const LogLine& logLine)
{
    return (
        (m_thread == logLine.m_thread)
        && (m_date == logLine.m_date)
        && (m_time == logLine.m_time)
        && (m_level == logLine.m_level)
        && (m_flag == logLine.m_flag)
        && (m_message == logLine.m_message)
    );
}

std::string LogLine::GetLogThread()
{
    return m_thread;
}

std::string LogLine::GetLogDate()
{
    return m_date;
}

std::string LogLine::GetLogTime()
{
    return m_time;
}

LogLevel LogLine::GetLogLevel()
{
    return m_level;
}

std::string LogLine::GetLogFlag()
{
    return m_flag;
}

std::string LogLine::GetLogMessage()
{
    return m_message;
}