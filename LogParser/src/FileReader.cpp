#include <FileReader.h>

FileReader::FileReader() :
    m_file(NULL),
    m_fileName(),
    m_bufSize(0),
    m_size(0),
    m_offset(0)
{
}

FileReader::FileReader(const std::string& fileName, size_t bufSize)
{
    m_file = fopen(fileName.c_str(), "rb");

    if (!m_file)
        throw std::runtime_error(std::string("Unavailible to open ") + fileName);

    m_fileName = fileName;

    fseek(m_file, 0, SEEK_END);
    m_size = ftell(m_file);
    fseek(m_file, 0, SEEK_SET);

    if (bufSize > m_size)
        m_bufSize = m_size;
    else
        m_bufSize = bufSize;

    m_offset = 0;
}

FileReader::~FileReader()
{
    fclose(m_file);
    m_fileName.clear();
    m_bufSize = 0;
    m_size = 0;
    m_offset = 0;
}

char* FileReader::ReadNext(char* buf, size_t& readSize)
{
    readSize = m_bufSize;

    if (m_offset >= m_size)
    {
        readSize = 0;
        return buf;
    }

    if (m_bufSize > (m_size - m_offset))
        readSize = m_size - m_offset;

    if (!buf)
        buf = new char[readSize];

    fread(buf, 1, readSize, m_file);

    m_offset += readSize;

    return buf;
}

size_t FileReader::GetBufferSize()
{
    return m_bufSize;
}