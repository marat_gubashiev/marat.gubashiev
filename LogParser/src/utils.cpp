#include "utils.h"

void utils::Split(std::vector<std::string>& dst, const std::string& src, const std::string& delimeter)
{
    size_t begin = 0;
    size_t end;
    size_t delimeterSize = delimeter.size();

    dst.clear();

    while ((end = src.find(delimeter, begin)) <= std::string::npos)
    {
        dst.push_back(src.substr(begin, end - begin));
        if (end == std::string::npos)
            break;
        begin = end + delimeterSize;
    }
}

const char* utils::FindSubstringInBuffer(const char* first, const char* last, const char* substr)
{
    const char* p1;
    const char* p2;
    
    if (!first)
        throw std::runtime_error("first is NULL");
    if (!last)
        throw std::runtime_error("last is NULL");
    if (!substr)
        throw std::runtime_error("substr is NULL");

    for (; first < last - strlen(substr); ++first)
    {
        if (*first == *substr)
        {
            p1 = first;
            p2 = substr;
            for (; *p2 != 0; ++p1, ++p2)
                if (*p1 != *p2)
                    break;
            if (*p2 == 0)
                return first;
        }
    }
    return last;
}