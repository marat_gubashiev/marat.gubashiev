#include "LogLevel.h"

std::string LogLevelToString(LogLevel level)
{
    return LogLevel_str[static_cast<int>(level)];
}

LogLevel StringToLogLevel(const std::string& level_str)
{
    auto it = std::find(LogLevel_str.begin(), LogLevel_str.end(), level_str);
    if (it != LogLevel_str.end())
        return static_cast<LogLevel>(distance(LogLevel_str.begin(), it));
    return LogLevel::LL_EMPTY;
}

std::ostream& operator<<(std::ostream& os, const LogLevel& logLevel)
{
    os << LogLevel_str[static_cast<int>(logLevel)];
    return os;
}