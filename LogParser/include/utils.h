#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <time.h>
#include <string>
#include <vector>
#include <exception>
#include <stdexcept>

/* Create timer with name [name] */
#define CREATE_TIMER(name) \
    clock_t start_##name, stop_##name
/* Start timer with name [name] */
#define START_TIMER(name) \
    start_##name = clock()
/* Stop timer with name [name] */
#define STOP_TIMER(name) \
    stop_##name = clock()
/* Calculate timer with name [name] in seconds */
#define TIMER_IN_SEC(name) \
    ((static_cast<double>(stop_##name) - start_##name) / CLOCKS_PER_SEC)
/* Print timer with name [name] in seconds */
#define PRINT_TIMER(name) \
    printf("\nElapsed time of %s %.3lf sec\n", #name, TIMER_IN_SEC(name))

/* namespace with some utilities */
namespace utils
{
    /**
     * @brief Split string by delimiter and save splited string to vector of strings
     *
     * @param dst vector of strings
     * @param src string
     * @param delimiter delimiter
     */
    void Split(std::vector<std::string>& dst, const std::string& src, const std::string& delimiter);

    /**
     * @brief Find the first occurrence of the substring in buffer
     * 
     * @param first pointer to the first element of the buffer
     * @param last pointer to the last element of the buffer
     * @param substr pointer to the substring (the substring must be null terminated string)
     * @return const char* the position of the first character of the first match.
     *                     if no matches were found, the function returns pointer to the last element of the buffer
     */
    const char* FindSubstringInBuffer(const char* first, const char* last, const char* substr);

    template<typename T>
    inline bool IsContains(const std::vector<T>& vector, const T& value)
    {
        return (std::find(vector.begin(), vector.end(), value) != vector.end());
    }
};

#endif