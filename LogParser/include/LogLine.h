#ifndef LOGLINE_H
#define LOGLINE_H

#include <ostream>
#include <string>
#include <exception>
#include <stdexcept>

#include "LogLevel.h"
#include "utils.h"

/* count digits of int number */
#define INT_LENGTH(d) \
    (floor(log10(abs(d))) + 1)

const int DATE_PARSED_ELEMENTS = 3;
const int TIME_PARSED_ELEMENTS = 4;

class LogLine;

/* Class contain representation of one log note (or line) */
class LogLine
{
/*
 * ~#[<m_thread>]; <m_date>; <m_time>; <m_level>; <m_flag>; <m_message>
 */
private:
    std::string m_thread;
    std::string m_date;
    std::string m_time;
    LogLevel m_level;
    std::string m_flag;
    std::string m_message;
public:
    /**
     * @brief Construct a new empty Log Line object
     * 
     */
    LogLine();
    /**
     * @brief Construct a new Log Line object or cause exception on any error
     *
     * @param line log line
     */
    LogLine(std::string& line);
    /**
     * @brief Construct a new Log Line object or cause exception on any error
     *
     * @param constLine log line
     */
    LogLine(const std::string& constLine);
    /**
     * @brief Destroy the Log Line object
     * 
     */
    ~LogLine();

public:
    friend std::ostream& operator<<(std::ostream& os, const LogLine& logLine);

public:
    bool operator!=(const LogLine& logLine);
    bool operator==(const LogLine& logLine);

public:
    /**
     * @brief Get the Log Thread
     * 
     * @return std::string log thread
     */
    std::string GetLogThread();
    /**
     * @brief Get the Log Date
     * 
     * @return std::string log date
     */
    std::string GetLogDate();
    /**
     * @brief Get the Log Time
     * 
     * @return std::string log time
     */
    std::string GetLogTime();
    /**
     * @brief Get the Log Level
     * 
     * @return LogLevel log level
     */
    LogLevel GetLogLevel();
    /**
     * @brief Get the Log Flag
     * 
     * @return std::string log flag
     */
    std::string GetLogFlag();
    /**
     * @brief Get the Log Message
     * 
     * @return std::string log message
     */
    std::string GetLogMessage();
};

#endif