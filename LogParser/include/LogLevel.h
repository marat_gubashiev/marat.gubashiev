#ifndef LOGLEVEL_H
#define LOGLEVEL_H

#include <ostream>
#include <string>
#include <vector>

/**
 * @brief enum representation of log levels
 */
enum class LogLevel
{
    LL_EMPTY,
    LL_TRACE,
    LL_INFO,
    LL_DEBUG,
    LL_WARN,
    LL_ERROR,
    LL_ALL
};

/**
 * @brief string representation of log levels
 * 
 */
const std::vector<std::string> LogLevel_str =
{
    "EMPTY",
    "TRACE",
    "INFO",
    "DEBUG",
    "WARN",
    "ERROR",
    "ALL"
};

/**
 * @brief Convert LogLevel to string
 *
 * @param level log level
 * @return string representation of log level
 */
std::string LogLevelToString(LogLevel level);
/**
 * @brief Convert string to LogLevel
 *
 * @param level_str string representation of log level
 * @return log level
 */
LogLevel StringToLogLevel(const std::string& level_str);


std::ostream& operator<<(std::ostream& os, const LogLevel& logLevel);

#endif