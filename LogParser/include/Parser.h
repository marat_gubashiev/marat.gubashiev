#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <ostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <initializer_list>
#include <algorithm>
#include <exception>
#include <stdexcept>

#include "FileReader.h"
#include "utils.h"
#include "LogLevel.h"
#include "LogLine.h"

class Parser;

/* Struct that contain parser results. Actualy contain info about parsed log file */
struct ParserResults
{
    double m_elapsedTime = 0.0;
    size_t m_parsedSuccesfully = 0;
    size_t m_countOfTrace = 0;
    size_t m_countOfInfo = 0;
    size_t m_countOfDebug = 0;
    size_t m_countOfWarn = 0;
    size_t m_countOfError = 0;
    std::string m_beginTime;
    std::string m_endTime;
};

/* Class that parse log file and filter and sort log lines */
class Parser
{
public:
    using iterator = std::vector<LogLine>::iterator;
private:
    std::string m_fileName;
    /* All log lines from log file */
    std::vector<LogLine> m_parsed;
    /* Filtered log lines */
    std::vector<LogLine> m_filtered;
    
    bool m_trace;
    bool m_info;
    bool m_debug;
    bool m_warn;
    bool m_error;
    bool m_sortByLevel;
    bool m_reverse;

    ParserResults m_parserResults;
public:
    /**
     * @brief Construct a new Parser object
     */
    Parser();
    /**
     * @brief Construct a new Parser object
     *
     * @param fileName name of log file
     */
    Parser(const std::string& fileName);
    /**
     * @brief Destroy the Parser object
     */
    ~Parser();
public:
    /**
     * @brief Return size of filtered log lines
     *
     * @return size of filtered log lines
     */
    size_t size() const;
    /**
     * @brief Overloading operator []
     *
     * @param index log line index in filtered log lines
     * @return LogLine& log line at index in filtered log lines
     */
    LogLine& operator[](size_t index);
    /**
     * @brief Overloading operator []
     *
     * @param index log line index in filtered log lines
     * @return const LogLine& log line at index in filtered log lines
     */
    const LogLine& operator[](size_t index) const;
    
    /**
     * @brief Returns an iterator to the beginning
     * 
     * @return iterator to the beginning
     */
    iterator begin();
    /**
     * @brief Returns an iterator to the end
     * 
     * @return iterator to the end
     */
    iterator end();
public:
    /**
     * @brief Filter and sort parsed log lines
     * 
     * @param trace Add log lines with log level TRACE to filter
     * @param info Add log lines with log level INFO to filter
     * @param debug Add log lines with log level DEBUG to filter
     * @param warn Add log lines with log level WARN to filter
     * @param error Add log lines with log level ERROR to filter
     * @param sortByLevel if (TRUE) { Sort by log level } else { Sort by time }
     * @param reverse Reverse sort
     */
    void Filter(
        bool trace = true,
        bool info = true,
        bool debug = true,
        bool warn = true,
        bool error = true,
        bool sortByLevel = false,
        bool reverse = false
    );

    /**
     * @brief Filter and sort parsed log lines
     * 
     * @param levels vector with log levels to filter
     * @param sortByLevel if (TRUE) { Sort by log level } else { Sort by time }
     * @param reverse Reverse sort
     */
    void Filter(const std::vector<LogLevel>& levels, bool sortByLevel = false, bool reverse = false);
public:
    /**
     * @brief Write filtered log lines to CSV file
     *
     * @param csvFileName csv file name
     */
    void WriteInCSV(const std::string& csvFileName = "");

public:
    friend std::ostream& operator<<(std::ostream& os, const Parser& parser);
};

std::ostream& operator<<(std::ostream& os, const std::unique_ptr<Parser>& parser_ptr);

#endif