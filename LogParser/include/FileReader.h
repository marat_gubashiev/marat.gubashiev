#ifndef FILE_READER_H
#define FILE_READER_H

#include <stdlib.h>
#include <stdio.h>

#include <string>
#include <exception>
#include <stdexcept>

const size_t DEFAULT_BUFFER_SIZE = 4096;

/**
 * @brief File Reader Class
 * 
 */
class FileReader
{
public:
    /**
     * @brief Construct a new File Reader object
     * 
     */
    FileReader();
    /**
     * @brief Construct a new File Reader object
     * 
     * @param fileName file name
     * @param bufSize block size
     */
    FileReader(const std::string& fileName, size_t bufSize = DEFAULT_BUFFER_SIZE);
    /**
     * @brief Destroy the File Reader object
     * 
     */
    ~FileReader();

public:
    /**
     * @brief 
     * 
     * @param buf 
     * @param readSize 
     * @return char* value of buf
     */
    char* ReadNext(char* buf, size_t& readSize);

public:
    /**
     * @brief Get the Buffer Size object
     * 
     * @return size_t 
     */
    size_t GetBufferSize();
    
private:
    FILE* m_file;
    std::string m_fileName;
    size_t m_size;
    size_t m_bufSize;
    size_t m_offset;
};

#endif